//require gulp
var gulp = require('gulp');
// require other packages
var sass = require('gulp-sass');

const scssDestPath = 'app/design/frontend/SW/VA/web/css';
const scssPath = scssDestPath + '/scss';

gulp.task('sass', function() {
    gulp.src(scssPath + '/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(scssDestPath))
});
